

from sshtunnel import SSHTunnelForwarder
from pymongo import MongoClient

# VM IP/DNS - Will depend on your VM
EC2_URL = 'ec2-35-183-40-11.ca-central-1.compute.amazonaws.com'

# Mongo URI Will depende on your DocDB instances
DB_URI = 'alexa-staging-translation-memory.cluster-ct7jyhh0jgpl.ca-central-1.docdb.amazonaws.com'

# DB user and password
DB_USER = 'admin093D15'
DB_PASS = 'Q9rMxLUt#lUfRa3Cp!'

# Create the tunnel
server = SSHTunnelForwarder(
    (EC2_URL, 22),
    ssh_username='ec2-user',                #
    ssh_pkey='staging-documentDB-ssh.pem',   # 
    remote_bind_address=(DB_URI, 59500),
    local_bind_address=('127.0.0.1', 27019)
)
# Start the tunnel
server.start()

# Connect to Database
client = MongoClient(
    host='127.0.0.1',
    port=27019,
    username='DB_USER',
    password='DB_PASS',
    #ssl='true',
  # ssl_ca_certs='./rds-combined-ca-bundle.pem',
  #ssl_cert_reqs=ssh.CERT_NONE,
  retryWrites='false'
)

try:
   # The ismaster command is cheap and does not require auth.
   client.admin.command('ismaster')
#   print("DB is Available")
except ConnectionFailure:
   print("Server not available")

#print(client.list_database_names())
# Close the tunnel once you are done
server.stop()
