import json

import boto3 as boto3
import requests,sys

request = requests.get('https://admin-tools.alexatranslations.com/login/?next=/')
# Write output from print to "result.log" temp file

orig_stdout = sys.stdout
f = open('result.log', 'w')
sys.stdout = f

if request.status_code == 200:
    print('Web site is ok')
else:
    print('Web site may be down!') 

sys.stdout = orig_stdout
f.close()

LOG = open('./result.log')
DATA = LOG.read()

def function_name(event, context):
    topic_name = ""
    sns = boto3.client('sns')

    existing_topics = sns.list_topics()['Topics']

    for t in existing_topics:
        # check for ARN in the specific topic
        if 'demo-2-topic-1628084731392217700' in t.get('TopicArn'):
            topic_name = t.get('TopicArn')
            break

    print(f"Found topic: {topic_name}")

    # send the message to the publisher
    sns.publish(
        TargetArn=topic_name,
        Message=DATA,
        Subject='Check Admin Tool')

    

    return {
        "statusCode": 200,
        "body": json.dumps({"message": "Ok"})
    }


if __name__ == '__main__':
    # Local testing
    function_name({}, None)
