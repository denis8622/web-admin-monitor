import json

import boto3 as boto3
import requests, sys

import ssl
import logging
import logging.handlers
from sshtunnel import SSHTunnelForwarder
from pymongo import MongoClient

# VM IP/DNS - Will depend on your VM
EC2_URL = 'ec2-35-183-40-11.ca-central-1.compute.amazonaws.com'

# Mongo URI Will depende on your DocDB instances
DB_URI = 'alexa-staging-translation-memory.cluster-ct7jyhh0jgpl.ca-central-1.docdb.amazonaws.com'

# DB user and password
DB_USER = 'admin093D15'
DB_PASS = 'Q9rMxLUt#lUfRa3Cp!'

# Create the tunnel
server = SSHTunnelForwarder(
    (EC2_URL, 22),
    ssh_username='ec2-user',  #
    ssh_pkey='staging-documentDB-ssh.pem',  #
    remote_bind_address=(DB_URI, 59500),
    local_bind_address=('127.0.0.1', 27019),

)

# Start the tunnel
server.start()

# Connect to Database
client = MongoClient(
    host='127.0.0.1',
    port=27019,
    username=DB_USER,
    password=DB_PASS,
    ssl='true',
    ssl_cert_reqs=ssl.CERT_NONE,
    retryWrites='false'
)


orig_stdout = sys.stdout
f = open('result.log', 'w')
sys.stdout = f
region_name = 'ca-central-1',
if client.admin.command('ismaster'):
    print("DB is Ok")
else:
    print("DB is not ok")


sys.stdout = orig_stdout
f.close()

LOG = open('./result.log')
DATA = LOG.read()


def function_name(event, context):
    topic_name = ""
    sns = boto3.client('sns')

    existing_topics = sns.list_topics()['Topics']

    for t in existing_topics:
        # check for ARN in the specific topic
        if 'demo-2-topic-1628084731392217700' in t.get('TopicArn'):
            topic_name = t.get('TopicArn')
            break

    print(f"Found topic: {topic_name}")

    # send the message to the publisher
    sns.publish(
        TargetArn=topic_name,
        Message=DATA,
        Subject='Check Admin Tool')

    return {
        "statusCode": 200,
        "body": json.dumps({"message": "Ok"})
    }


if __name__ == '__main__':
    # Local testing
    function_name({}, None)